# Project config #

Install and activate virtualenv

```
#!bash
virtualenv -p /usr/bin/python2.7 .env
. .env/bin/activate
```
Install requirements

```
#!bash
pip install -r requirements.txt
```
Create database

```
#!bash
./manage.py migrate
```

Create superuser

```
#!bash
./manage.py createsuperuser
```

Run tests

```
#!bash
./manage.py test
```

Load initial data

```
#!bash
./manage.py loaddata student/fixtures/initial_data.json
```

Use command

```
#!bash
./manage.py show_students
```

Start local server

```
#!bash
./manage.py runserver
```

After this site will be avialable on http://127.0.0.1:8000
