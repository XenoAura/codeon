from django.contrib import admin
from student.models import Student, StudentGroup
from django.utils.translation import ugettext_lazy as _


class StudentInline(admin.TabularInline):
    model = Student


class StudentGroupAdmin(admin.ModelAdmin):
    inlines = [StudentInline, ]

    list_display = ('id', 'name', 'students_count')
    list_display_links = ('id', 'name',)
    search_fields = ('name', )

    def students_count(self, obj):
        return obj.students_of_group.count()

    def get_form(self, request, obj=None, **kwargs):
        form = super(StudentGroupAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['headman'].queryset = Student.objects.filter(group=obj)
        return form

    students_count.short_description = _('Students count')


admin.site.register(StudentGroup, StudentGroupAdmin)
