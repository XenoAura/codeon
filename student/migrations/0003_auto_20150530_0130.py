# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0002_auto_20150529_2203'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='group',
            field=models.ForeignKey(related_name='students_of_group', verbose_name='Group', to='student.StudentGroup'),
        ),
        migrations.AlterField(
            model_name='studentgroup',
            name='headman',
            field=models.OneToOneField(null=True, blank=True, to='student.Student', verbose_name='Headman'),
        ),
        migrations.AlterField(
            model_name='studentgroup',
            name='name',
            field=models.CharField(max_length=30, verbose_name='Name'),
        ),
    ]
