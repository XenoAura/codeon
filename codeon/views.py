from django.contrib.auth import authenticate, login
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from codeon.forms import RegistrationForm
from django.template import RequestContext


def registration(request):
    form = RegistrationForm(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        form.save()
        user = authenticate(username=form_data['username'], password=form_data['password'])
        if user is not None:
            login(request, user)
        return HttpResponseRedirect(reverse_lazy('home'))
    return render_to_response('registration.html', {'form': form}, context_instance=RequestContext(request))


def home(request):
    return render_to_response('index.html', {}, context_instance=RequestContext(request))