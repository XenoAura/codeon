import time

from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import connection


# Template context processor
def get_settings(request):
    return dict(settings=settings)


# Authentication backend
class EmailOrUsernameModelBackend(object):
    """
    This is a ModelBacked that allows authentication with either a username or an email address.

    """
    def authenticate(self, username=None, password=None):
        if '@' in username:
            kwargs = {'email': username}
        else:
            kwargs = {'username': username}
        try:
            user = get_user_model().objects.get(**kwargs)
            if user.check_password(password):
                return user
        except get_user_model().DoesNotExist:
            return None

    def get_user(self, username):
        try:
            return get_user_model().objects.get(pk=username)
        except get_user_model().DoesNotExist:
            return None


# Midlleware
class CodeonMiddleware(object):

    def process_request(self, request):
        self.time = time.time()

    def process_response(self, request, response):
        request_time = "<p>Total time: {}</p>".format(time.time() - self.time)
        query_count = "<p>Query count: {}</p>".format(len(connection.queries))
        try:
            begin, end = response.content.split("</body>")
            response.content = "\n".join([begin, request_time, query_count, "</body>", end])
        except Exception:
            pass
        return response
