# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase
from django.contrib.auth import get_user_model
from django.test import Client
from django.core.urlresolvers import reverse_lazy

from student.models import Student, StudentGroup

User = get_user_model()

TEST_USER = {
    'username': 'faker',
    'email': 'faker@example.com',
    'password': 'super_faker'
}

TEST_GROUP = {
    'name': 'ИФ-08-1'
}

TEST_STUDENT = {
    'first_name': 'Андрей',
    'last_name': u'Петрович',
    'middle_name': 'Иваниц',
    'birthday': '1990-01-01',
    'ticket': '088020',
}


class AnimalTestCase(TestCase):
    def setUp(self):
        User.objects.create_user(**TEST_USER)

    def test_login_and_add_objects(self):
        client = Client()

        print '#'*50
        print 'Trying login by email'
        response = client.post(reverse_lazy('login'),
            {'username': TEST_USER['email'], 'password': TEST_USER['password']})
        self.assertEqual(response.status_code, 302)
        print response.status_code

        print '#'*50
        print 'Logout test user'
        response = client.post(reverse_lazy('logout'), {})
        self.assertEqual(response.status_code, 302)
        print response.status_code

        print '#'*50
        print 'Trying login by username'
        response = client.post(reverse_lazy('login'),
            {'username': TEST_USER['username'], 'password': TEST_USER['password']})
        self.assertEqual(response.status_code, 302)
        print response.status_code

        print '#'*50
        print 'Trying create a new group'
        response = client.post(reverse_lazy('student:group_add'), TEST_GROUP)
        group = StudentGroup.objects.get(**TEST_GROUP)
        self.assertEqual(group.name, TEST_GROUP['name'])
        self.assertEqual(response.status_code, 302)
        print response.status_code

        print '#'*50
        print 'Trying create a new student'
        TEST_STUDENT['group'] = group.pk
        file = open('media/students_photo/test_goose.jpg')
        TEST_STUDENT['photo'] = file
        response = client.post(reverse_lazy('student:student_add'), TEST_STUDENT)
        print response.status_code
        file.close()

        student = Student.objects.get(ticket=TEST_STUDENT['ticket'])

        self.assertEqual(response.status_code, 302)
        self.assertEqual(student.first_name, TEST_STUDENT['first_name'])
        self.assertEqual(student.last_name, TEST_STUDENT['last_name'])
        self.assertEqual(student.middle_name, TEST_STUDENT['middle_name'])
        self.assertEqual(student.ticket, TEST_STUDENT['ticket'])
        self.assertEqual(student.group.pk, TEST_STUDENT['group'])
        self.assertEqual(student.birthday.strftime('%Y-%m-%d'), TEST_STUDENT['birthday'])

        # Delete Student object for delete test image
        student.delete()
