from django import forms
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _

User = get_user_model()


class RegistrationForm(forms.ModelForm):
    first_name = forms.CharField(max_length=30, min_length=1, required=True, label=_('First name:'))
    last_name = forms.CharField(max_length=30, min_length=1, required=True, label=_('Last name:'))
    email = forms.EmailField(max_length=50, required=True, label=_('Email:'))
    username = forms.CharField(max_length=30, min_length=4, required=True, label=_('Login:'))
    password = forms.CharField(
        widget=forms.PasswordInput(), min_length=6, max_length=30, required=True, label=_('Password:'))
    password_again = forms.CharField(
        widget=forms.PasswordInput(), min_length=6, max_length=30, required=True, label=_('Confirm password:'))

    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'email',
            'username',
            'password',
            'password_again',
        )

    def clean_username(self):
        if User.objects.filter(username=self.cleaned_data.get('username')).exists():
            raise forms.ValidationError(_("Username is busy!"))
        if '@' in self.cleaned_data.get('username'):
            raise forms.ValidationError(_("Username can't contain @ symbol!"))
        return self.cleaned_data.get('username')

    def clean_email(self):
        if User.objects.filter(email=self.cleaned_data.get('email')).exists():
            raise forms.ValidationError(_("Email is busy!"))
        return self.cleaned_data.get('email')

    def clean(self):
        cleaned_data = super(RegistrationForm, self).clean()
        if cleaned_data.get('password') is None or cleaned_data.get('password') != cleaned_data.get('password_again'):
            raise forms.ValidationError(_("Pasword mismatch!"))
        return cleaned_data

    def save(self, commit=True):
        new_user = User.objects.create_user(
            username=self.cleaned_data['username'],
            email=self.cleaned_data['email'],
            password=self.cleaned_data['password']
        )
        new_user.first_name = self.cleaned_data['first_name']
        new_user.last_name = self.cleaned_data['last_name']
        new_user.save()
